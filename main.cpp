#include <mbed.h>
#include <random>
Serial pc(USBTX, USBRX); // tx, rx
DigitalIn button(D4);

DigitalOut green(D5);
DigitalOut yellow(D6);

std::random_device r;
std::mt19937 mt(r());
std::uniform_real_distribution<> distrib(0, 5); 

Timeout launcher;
Timer counter;

void start();
void play();

float measure = -1;

int main(){
    pc.baud(9600);
    pc.printf("Bienvenue dans le jeu !\n");

    while (true){
        start();
        while (measure == -1) thread_sleep_for(1000);
        pc.printf("Temps de reaction : %f\n", measure);
        measure = -1;
    }
    return 0;
}

void start()
{
    yellow = 1;
    green = 1;
    // Demarrage du jeu (attente d'un appuie)
    while(!button);

    pc.printf("Demarrage\n");
    // Attente du relachement
    while (button);

    pc.printf("Relachement\n");
    yellow = 0;
    green = 0;

    auto duration = distrib(mt);

    pc.printf("Temps d'attente : %f\n", duration);
    launcher.attach(&play, duration);
}

void play()
{
    yellow = 1;
    counter.start();
    
    // Attente de l'appuie sur le bouton
    while (!button);

    counter.stop();
    yellow = 0;
    green = 1;

    //Attente du relachement du bouton
    while (button);

    // Calcul du temps
    measure = counter.read();
    counter.reset();
}