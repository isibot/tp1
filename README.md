# TP1

Découverte de la robotique.

Appuyer sur un bouton quand la LED s'allume.

Allumer/Eteindre LED.
Récupérer information bouton.
Timer, Timeout.
Affichage via le serial.

# Mbed-cli

C'est un outil en ligne de commande permettant de simplifier la compilation de code en C/C++ pour les plateformes mobiles supportées par mbed. Il fournit également l'ensemble des fonctionnalités du framework mbed (accès aux pins, protocoles de communication, gestion des interruptions...).

Il permet de sélectionner le compilateur utilisé, et donc de sélectionner la version du standard C++ voulue. La version utilisée ici est le c++ 17 compilé avec gcc en version 9.3.


## Installation sous Ubuntu

On présente ici l'installation sous Ubuntu, mais c'est possible de l'installer pour n'importe quelle version de Linux supportant python3.

Il vous faudra donc installer python3, pip, git et mercurial.

```bash
sudo apt update
sudo apt install -y python3 python3-pip git mercurial
sudo pip3 install mbed-cli
```

Télécharger le compilateur :

https://developer.arm.com/tools-and-software/open-source-software/developer-tools/gnu-toolchain/gnu-rm/downloads

On va maintenant ajouter le compilateur gcc pour ARM.

```
tar xvf gcc-arm-noe-eabi.tar.bz
export MBED_GCC_ARM_PATH="/path/to/gcc/arm/bin"
```

Il faut redémarrer le terminal.
Il ne reste qu'à créer le projet et à le paramètré.
```
mbed new --program tp1
cd tp1
mbed target "NUCLEO_F401RE"
mbed toolchain "GCC_ARM"
```